# vmol-acc-spcache

Moleculer mixin adding Salepoint cache access to the local service.

## Configuration

Settings can be provided by the service or using enviroment variables.

```
    settings: {
        s3: {
            spcacheFields: ['id', 'slug', 'name'],
            spcacheOptions: {
                services: false
            }
        }
    }
```

## Usage

To enable, add the middleware to your services.

```
const AccSalepointCacheService = require('vmol-acc-spcache');

module.exports = {
    name: 'myservice',

    /**
     * Mixins
     */
    mixins: [ AccSalepointCacheService ],
};

```

## API

### spcacheGet(ctx, salepointId, fieldToFetch)
### spcacheConfig(ctx, salepointId, fieldToFetch)
