'use strict';

const lodash = require('lodash');
const { LRUCache } = require('lru-cache');

/**
 * Service definition
 */
module.exports = {

    settings: {
        spcacheMax: 1000,
        spcacheMaxAge: 60 * 60, // 1h
        spcacheConfigFields: [],
        spcacheFields: ['id', 'slug', 'name'],
        spcacheOptions: {
            services: false,
            schedules: false
        },
        salepointService: 'salepoints'
    },

    /**
     * Events
     */
    events: {
        'salepoint.created': {
            params: {
                $$strict: false,
                id: 'string|numeric'
            },
            async handler(ctx) {
                this.logger.info(`Reload salepoint after create ${ctx.params.id}`);
                await this._spcacheLoad(ctx, ctx.params.id);
            }
        },

        'salepoint.updated.*': {
            params: {
                $$strict: false,
                id: 'string|numeric',
                field: 'string',
                value: 'any'
            },
            async handler(ctx) {
                if (this.settings.spcacheFields.indexOf(ctx.params.field) === -1) {
                    return;
                }
                this.logger.info(`Reload salepoint after update ${ctx.params.id}`);
                await this._spcacheLoad(ctx, ctx.params.id);
            }
        },

        'service.created': {
            params: {
                $$strict: false,
                salepoint_id: 'string|numeric'
            },
            async handler(ctx) {
                if (!this.settings.spcacheOptions.services) {
                    return;
                }
                this.logger.info(`Reload salepoint after service create ${ctx.params.salepoint_id}`);
                await this._spcacheLoad(ctx, ctx.params.salepoint_id);
            }
        },

        'service.updated': {
            params: {
                $$strict: false,
                salepoint_id: 'string|numeric'
            },
            async handler(ctx) {
                if (!this.settings.spcacheOptions.services) {
                    return;
                }
                this.logger.info(`Reload salepoint after service update ${ctx.params.salepoint_id}`);
                await this._spcacheLoad(ctx, ctx.params.salepoint_id);
            }
        },

        'salepoint-config.updated': {
            params: {
                $$strict: false,
                salepoint_id: 'string|numeric'
            },
            async handler(ctx) {
                this.logger.info(`Reload salepoint config ${ctx.params.salepoint_id}`);
                await this._spcacheConfigLoad(ctx, ctx.params.salepoint_id);
            }
        }
    },

    started() {
        this._spcache = new LRUCache({
            max: this.settings.spcacheMax,
            ttl: this.settings.spcacheMaxAge * 1000
        });
        this._spconfig = new LRUCache({
            max: this.settings.spcacheMax,
            ttl: this.settings.spcacheMaxAge * 1000
        });
    },

    /**
     * Methods
     */
    methods: {
        async spcacheGet(ctx, id, field) {
            this.logger.info(`Get salepoint data [${id}] [${field}]`);

            const sp = this._spcache.get(id);
            if (field && lodash.get(sp, field) !== undefined) {
                return lodash.get(sp, field);
            }
            if (!field && sp) {
                return sp;
            }

            const salepoint = await this._spcacheLoad(ctx, id);
            return field ? lodash.get(salepoint, field) : salepoint;
        },

        async spcacheConfig(ctx, id, field) {
            this.logger.info(`Get salepoint config [${id}] [${field}]`);

            const conf = this._spconfig.get(id);
            if (field && lodash.get(conf, field) !== undefined) {
                return lodash.get(conf, field);
            }
            if (!field && conf) {
                return conf;
            }

            const c = await this._spcacheConfigLoad(ctx, id);
            return field ? lodash.get(c, field) : c;
        },

        async _spcacheLoad(ctx, id) {
            this.logger.info(`Load salepoint [${id}]`);

            const res = await ctx.call(`${this.settings.salepointService}.get`, {
                id: id,
                include_services: this.settings.spcacheOptions.services,
                include_schedules: this.settings.spcacheOptions.schedules
            });
            const salepoint = lodash.pick(res, this.settings.spcacheFields);

            this.logger.info(`Set salepoint in cache [${id}]`);
            this._spcache.set(id, salepoint);

            return salepoint;
        },

        async _spcacheConfigLoad(ctx, id) {
            this.logger.info(`Load salepoint config [${id}]`);

            const res = await ctx.call(`${this.settings.salepointService}.getConfig`, { id: id });
            const c = lodash.pick(res, this.settings.spcacheConfigFields);

            this.logger.info(`Set salepoint config in cache [${id}]`);
            this._spconfig.set(id, c);

            return c;
        }
    }
};
